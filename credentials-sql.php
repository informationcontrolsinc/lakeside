<?php
function aodCredentials($uri)
{
	global $credentials;
	
	if (isset($credentials[$uri])) {
		$output = $credentials[$uri];
	} else {
		$output = false;
	}
	
	return $output;
}

function companyIDToURI($cid)
{
	global $companyids;

	if (isset($companyids[$cid])) {
		$uri = $companyids[$cid];
	} else {
		$uri = false;
	}

	return $uri;
}

/* Grab data from SQL to be used as lookups for file */

$mysqli = new mysqli("mysql01", "root", "27lZr54R8qg2DIn", "ici_lakeside");
if ($mysqli->connect_errno) {
	trigger_error("Could not connect to the MySQL Server.", E_USER_NOTICE);
}
	
$credentials = array();
$query = "select * from `credentials`";
$queryResults = $mysqli->query($query);
while ($row = $queryResults->fetch_assoc()) {
	$credentials[$row["aoduri"]] = array("username" => $row["aoduser"], "password" => $row["aodpass"]);	
}

$companyids = array();
$query = "select * from `companyids`";
$queryResults = $mysqli->query($query);
while ($row = $queryResults->fetch_assoc()) {
	$companyids[$row["companyid"]] = $row["aoduri"];
}

?>