var activeuri = "";

$(function() {
	myInit();
});

function myInit() {
	$("#uri-list").on("click", "li", function() {
		x = this;
		var uri = $(this).text();
		if (uri == "Add new...") {
			newURI();
		} else {
			activeuri = uri;
			li_focused = this;
			loadDetails(uri);
		}
	});
	
	$("#sv-details-save").on("click", function() {
		saveDetails();
	});

	$("#new-company-id-btn").on("click", function() {
		addNewCompanyID();
	});

	$("#remove-company-id-btn").on("click", function() {
		removeCompanyID();
	});

	loadURIs();
}


function loadURIs(uri) {
	if (uri == null) uri = "";
	activeuri = uri;
	$("#uri-list").sdLoadingOverlay("show");
	$("#uri-list ul:eq(0)").empty();
	$.post("functions/config-load-uris.php", function(data) {
		$.each(data.uris, function(index, value){
			$("#uri-list ul:eq(0)").append("<li class='sv-li "+ (activeuri == value ? " selected" : "") +"'>" + value + "</li>");
		});
		$("#uri-list").sdLoadingOverlay("hide");
		$("li.sv-li").disableSelection();
		if (activeuri != "") {
			loadDetails(uri);
		}
	}, "json");
};

function newURI() {
	var name = prompt("Enter the AOD URI");
	if ((name != null) && (name != ""))	{
		$.post("functions/config-new-uri.php", {uri: name}, function(data) {
			loadURIs(data.uri);
		}, "json");
	}
}

function loadDetails(uri) {
	clearDetails();
	$("#detail-sheet").show();
	$("#detail-sheet").sdLoadingOverlay("show");
	$("button").removeAttr("disabled");
	$.post("functions/config-load-details.php", {uri: uri}, function(data) {
		$("#active-uri").text(uri);
		$("#aod-username").val(data.user);
		$("#aod-password").val(data.pass);
		$.each(data.ids, function(index, value){
			$("#company-ids").append(value + "<br>");
		});
		$("#detail-sheet").sdLoadingOverlay("hide");
	}, "json");
}

function clearDetails() {
	$("#aod-username").val("");
	$("#aod-password").val("");
	$("#company-ids").empty();
}

function saveDetails() {
	$("#sv-save-response").text("Saving...").show();
	$.post("functions/config-save-details.php", {
		aoduri: $("#uri-list li.selected").text(),
		aoduser: $("#aod-username").val(),
		aodpass: $("#aod-password").val()
	}, function(data) {
		$("#sv-save-response").text(data.response);
		setTimeout("$('#sv-save-response').fadeOut()", 1000);
	}, "json");	
}

function addNewCompanyID() {
	if (id = prompt("Enter the Plant Number for employees to include send to this URI", "")) {
		var uri = $("#uri-list li.selected").text();
		$.post("functions/config-add-companyid.php", {uri: uri, id: id}, function(data){
			loadDetails(data.uri);
		}, "json");
	}
}

function removeCompanyID() {
	if (id = prompt("Enter the Plant Number that should be removed from this URI. This is a security step to prevent accidental removal of an active ID.", "")) {
		var uri = $("#uri-list li.selected").text();
		$.post("functions/config-remove-companyid.php", {uri: uri, id: id}, function(data){
			loadDetails(data.uri);
		}, "json");
	}
}