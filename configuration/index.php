<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Lakeside Foods Interface Config</title>
		<script type="text/javascript" src="https://www.iciaod.com/base/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="https://www.iciaod.com/base/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script type="text/javascript" src="https://www.iciaod.com/base/splitview/splitview.js"></script>
		<script type="text/javascript" src="js/ici.js"></script>
		<link type="text/css" href="https://www.iciaod.com/base/css/Aristo/Aristo.css" rel="stylesheet" />
		<link type="text/css" href="https://www.iciaod.com/base/splitview/splitview.css" rel="stylesheet" />
		<link type="text/css" href="css/extra.css" rel="stylesheet" />
	</head>
<?php
	$productid = 2;
	include("/var/www/html/iciaod.com/services/login/functions/checklogin.php");
	
	if ($loginValid === false) {
		include("/var/www/html/iciaod.com/services/login/inlinelogin.php");
		die();
	}	
?>	
	<body>
		<div class="sv-scroll-list-container" id="uri-list">
			<div class="sv-scroll-list">
				<div class="sv-ul-title">URIs</div>
				<ul class="sv-ul">
				</ul>
			</div>
		</div>
		<div class="sv-detail-sheet" id="detail-sheet">
			<div class="sv-sheet-title">
				<span>Details</span>
				<button id="sv-details-save">Save Changes</button>
				<span id="sv-save-response"></span>
			</div>
			<div id="config-form">
				<p id="active-uri"></p>
				<div class="sv-details-section" id="device-uri-info">
					<div class="sv-section-title">Credentials</div>
					<div><label for="aod-username">Username</label><input type="text" id="aod-username" /></div>
					<div><label for="aod-password">Password</label><input type="text" id="aod-password" /></div>
				</div>
				<div class="sv-details-section" id="device-profile-section">
					<div class="sv-section-title">Plant Numbers (WG1)</div>
					<div id="company-ids"></div>
					<div><button id="new-company-id-btn">Add Plant Number</button><button id="remove-company-id-btn">Remove Plant Number</button></div>
				</div>
			</div>			
		</div>			
	</body>
</html>
