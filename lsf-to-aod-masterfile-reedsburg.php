<?php
// This file is filtered to only include lines from plant number 1706 - Reedsburg

echo __FILE__." running\n";
$masterfilename = @$argv[1];
trigger_error(__FILE__." running $masterfilename", E_USER_NOTICE);
require_once "base/ws/aod.php";	// Contains AOD Web Service Calls

function formatActiveStatus($status)
{
	$status = strtoupper($status);
	switch ($status) {
		case '0':
        case 'A':
			return 0;
        case '1':
        case 'T':
			return 1;
		default:
			return 0;		
	}
}

// Last Name Suffix,First Name,Middle Name,User ID,SSN,Badge,PayClass,WG1,WG2,WG3,Custom 1,Custom 2,
// Custom 3,Custom 4,Custom 5,Custom 6,SchPatt,Active Status,Termination Date,Hire Date,WGDate,HourlyStatus,FullTimeDate,
// Pay Type,Rate,RateDate,SalaryHours,Address1,Address2,City,State,Zip,Phone1,ClockGroup,Phone2,Union,Birth Date,SchPattDate

function iciParseMasterFile($timebank)
{
	$csvfile = explode("\n", str_replace("\r", "", trim($timebank)));
	
	if (count($csvfile) == 1) {
		/* Header only, nothing to process */
		return false;
	}
	
	$csvfile[0] = preg_replace("/\"/", "", $csvfile[0]);
	$header = explode(",", preg_replace("/\s+/", "", trim(strtolower($csvfile[0]), "\"\r\n")));
	array_shift($csvfile);
	
	foreach ($csvfile as $lineraw) {
		$lineraw = trim($lineraw, "\r\n");
/* 		$line = str_replace(",", "\t", $line); */
		$line = "";
		$inQuotes = false;
		for ($i = 0; $i < strlen($lineraw); $i++) {
			$char = $lineraw[$i];
			if (($char == ",") && ($inQuotes == false)) {
				$line .= "\t";
			} elseif ($char == "\"") {
				$inQuotes = !$inQuotes;
			} else {
				$line .= $char;
			}
		}

		$parsedline = explode("\t", $line);
		$parsedline = array_combine($header, $parsedline);

		if ($parsedline["wg1"] == "1706") { 
			if ($parseduri = companyIDToURI($parsedline["wg1"])) {
				$result[$parseduri][] = $parsedline;
			}
		}
	}
	
	return $result;
};

function filterDuplicateRecords(&$emprecords)
{
	$filtered = array();
	
	for ($i = 0; $i < count($emprecords); $i++) {
		$inFiltered = false;
		foreach ($filtered as &$filteredRecord) {
			if ($filteredRecord['userid'] == $emprecords[$i]['userid']) {
				$inFiltered = true;
				if ($emprecords[$i]['activestatus'] == 0) {
					$filteredRecord = $emprecords[$i];
				}
			}
		}
		if ($inFiltered === false) { $filtered[] = $emprecords[$i]; }
	}

	return $filtered;
}

include_once("credentials-sql.php");

$masterfile = file_get_contents($masterfilename);
$masterfileByURI = iciParseMasterFile($masterfile);

$emailBody = array();
$uriCounts = array();

foreach ($masterfileByURI as $uri => $parsedmf) {
	if ($aod_credentials = aodCredentials($uri)) {
		$aod_uri      	= $uri;
		$aod_username 	= $aod_credentials["username"];
		$aod_password 	= $aod_credentials["password"];
		
		$aod = new iciAodWebServices($aod_uri, $aod_username, $aod_password);

		$parsedmf = filterDuplicateRecords($parsedmf);
		
		foreach($parsedmf as $record) {
			if (@$record["skip"] == true) { break; } /* This record has been flagged to skip, do not process it. */

            $employeeDetail2Plus = $aod->TAeEmployeeDetail2Plus();
            $employeeDetail2Plus["LastName"] = $record['lastnamesuffix'];
            $employeeDetail2Plus["FirstName"] = $record['firstname'];
            $employeeDetail2Plus["Initial"] = $record['middlename'];
            $employeeDetail2Plus["EmpID"] = $record['userid'];
            $employeeDetail2Plus["WG1Code"] = $record['wg1'];
            $employeeDetail2Plus["StaticCustom2"] = $record["custom2"];
            $employeeDetail2Plus["StaticCustom3"] = $record["custom3"];
            $employeeDetail2Plus["StaticCustom4"] = $record["custom4"];
            $employeeDetail2Plus["StaticCustom5"] = $record["custom5"];
            $employeeDetail2Plus["ActiveStatus"] = formatActiveStatus($record['activestatus']);
            $employeeDetail2Plus["ActiveStatusConditionEffDate"] = ((formatActiveStatus($record['activestatus']) == 1) ? $record['terminationdate'] : '');
            $employeeDetail2Plus["DateOfHire"] = $record['hiredate'];
            $employeeDetail2Plus["HourlyStatusID"] = $record["hourlystatus"];
            $employeeDetail2Plus["CurrentRate"] = $record['rate'];
            $employeeDetail2Plus["CurrentRateEffDate"] = $record['ratedate'];
            $employeeDetail2Plus["UnionCode"] = $record["union"];

            
            set_time_limit(0); // This is to prevent timeouts. 0 should be no limit, but calling set_time_limit explicitly resets any timeout counts
        
            
            $result = $aod->editEmployeeDetail($employeeDetail2Plus);
						
		}
	}
}
